package db.dao;

import db.model.WorkerEntity;

import java.util.List;

public interface WorkerDAO {
    WorkerEntity findById(Long id);

    void save(WorkerEntity worker);

    void update(WorkerEntity worker);

    void delete(WorkerEntity worker);

    List<WorkerEntity> findAll();
}
