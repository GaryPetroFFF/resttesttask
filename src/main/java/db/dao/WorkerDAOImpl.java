package db.dao;

import db.model.WorkerEntity;
import db.util.DataSourceSingletone;
import db.util.WorkerColumnEnum;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class WorkerDAOImpl implements WorkerDAO {
    private NamedParameterJdbcTemplate template;
    private RowMapper<WorkerEntity> workerEntityRowMapper_ = new BeanPropertyRowMapper<WorkerEntity>(WorkerEntity.class);
    private final String FIND_BY_ID_SQL = "SELECT * FROM workers WHERE id = :id";
    private final String SAVE_SQL = "INSERT INTO workers (name, surname, work_days, status, last_vacation_date, on_work, department_id) " +
            "VALUES (:name, :surname, :workDays, :status, :lastVacationDate, :onWork, :departmentId)";
    private final String DELETE_SQL = "DELETE FROM workers WHERE id = :id";
    private final String UPDATE_SQL = "UPDATE workers " +
            "SET name = :name, surname = :surname, work_days = :workDays, status = :status, " +
            "last_vacation_date = :lastVacationDate, on_work = :onWork, department_id = :departmentId " +
            "WHERE id = :id";
    private final String FIND_ALL_SQL = "SELECT * FROM workers";

    public WorkerDAOImpl() {
        template = new NamedParameterJdbcTemplate(DataSourceSingletone.getDataSource());
    }

    public WorkerEntity findById(Long id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id", id);
        WorkerEntity worker = template.queryForObject(FIND_BY_ID_SQL, paramsMap, workerEntityRowMapper_);
        worker.setId(id);
        return worker;
    }

    public void save(WorkerEntity worker) {
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(WorkerColumnEnum.NAME.getName(), worker.getName())
                .addValue(WorkerColumnEnum.SURNAME.getName(), worker.getSurname())
                .addValue(WorkerColumnEnum.WORK_DAYS.getName(), worker.getWorkDays())
                .addValue(WorkerColumnEnum.STATUS.getName(), worker.getStatus())
                .addValue(WorkerColumnEnum.LAST_VACATION_DATE.getName(), worker.getLastVacationDate())
                .addValue(WorkerColumnEnum.IS_ON_WORK.getName(), worker.isOnWork())
                .addValue(WorkerColumnEnum.DEPARTMENT_ID.getName(), worker.getDepartmentId());
        template.update(SAVE_SQL, params, holder, new String[] { WorkerColumnEnum.ID.getName() });
        worker.setId(holder.getKey().longValue());
    }

    public void update(WorkerEntity worker) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put(WorkerColumnEnum.NAME.getName(), worker.getName());
        paramsMap.put(WorkerColumnEnum.SURNAME.getName(), worker.getSurname());
        paramsMap.put(WorkerColumnEnum.WORK_DAYS.getName(), worker.getWorkDays());
        paramsMap.put(WorkerColumnEnum.STATUS.getName(), worker.getStatus());
        paramsMap.put(WorkerColumnEnum.LAST_VACATION_DATE.getName(), worker.getLastVacationDate());
        paramsMap.put(WorkerColumnEnum.IS_ON_WORK.getName(), worker.isOnWork());
        paramsMap.put(WorkerColumnEnum.DEPARTMENT_ID.getName(), worker.getDepartmentId());
        paramsMap.put(WorkerColumnEnum.ID.getName(), worker.getId());
        template.update(UPDATE_SQL, paramsMap);
    }

    public void delete(WorkerEntity worker) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put(WorkerColumnEnum.ID.getName(), worker.getId());
        template.update(DELETE_SQL, paramsMap);
    }

    public List<WorkerEntity> findAll() {
        return template.query(FIND_ALL_SQL, workerEntityRowMapper_);
    }
}
