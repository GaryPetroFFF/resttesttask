package db.dao;

import db.model.DepartmentEntity;

import java.util.List;

public interface DepartmentDAO {
    DepartmentEntity findById(Long id);

    void save(DepartmentEntity department);

    void update(DepartmentEntity department);

    void delete(DepartmentEntity department);

    List<DepartmentEntity> findAll();
}
