package db.dao;

import db.model.DepartmentEntity;
import db.model.WorkerEntity;
import db.util.DataSourceSingletone;
import db.util.DepartmentColumnEnum;
import db.util.WorkerColumnEnum;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DepartmentDAOImpl implements DepartmentDAO {
    private NamedParameterJdbcTemplate template;
    private RowMapper<DepartmentEntity> departmentEntityRowMapper_ = new BeanPropertyRowMapper<DepartmentEntity>(DepartmentEntity.class);
    private final String FIND_BY_ID_SQL = "SELECT * FROM departments WHERE id = :id";
    private final String SAVE_SQL = "INSERT INTO departments (name) VALUES (:name)";
    private final String DELETE_SQL = "DELETE FROM departments WHERE id = :id";
    private final String UPDATE_SQL = "UPDATE departments SET name = :name WHERE id = :id";
    private final String FIND_ALL_SQL = "SELECT * FROM departments";

    public DepartmentDAOImpl() {
        template = new NamedParameterJdbcTemplate(DataSourceSingletone.getDataSource());
    }

    public DepartmentEntity findById(Long id) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("id", id);
        DepartmentEntity department = template.queryForObject(FIND_BY_ID_SQL, paramsMap, departmentEntityRowMapper_);
        department.setId(id);
        return department;
    }

    public void save(DepartmentEntity department) {
        KeyHolder holder = new GeneratedKeyHolder();
        SqlParameterSource params = new MapSqlParameterSource()
                .addValue(DepartmentColumnEnum.NAME.getName(), department.getName());
        template.update(SAVE_SQL, params, holder, new String[] {DepartmentColumnEnum.ID.getName()});
        department.setId(holder.getKey().longValue());
    }

    public void update(DepartmentEntity department) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put(DepartmentColumnEnum.NAME.getName(), department.getName());
        paramsMap.put(DepartmentColumnEnum.ID.getName(), department.getId());
        template.update(UPDATE_SQL, paramsMap);
    }

    public void delete(DepartmentEntity worker) {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put(DepartmentColumnEnum.ID.getName(), worker.getId());
        template.update(DELETE_SQL, paramsMap);
    }

    public List<DepartmentEntity> findAll() {
        return template.query(FIND_ALL_SQL, departmentEntityRowMapper_);
    }
}
