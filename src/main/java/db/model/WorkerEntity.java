package db.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "workers")
public class WorkerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @Column(name = "name")
    private String name;
    @Column(name = "surname")
    private String surname;
    @Column(name = "work_days")
    private int workDays;
    @Column(name = "status")
    private String status;
    @Column(name = "last_vacation_date")
    private Date lastVacationDate;
    @Column(name = "on_work")
    private Boolean onWork;
    @ManyToOne
    @JoinColumn(name="department_id", nullable=false)
    private int departmentId;

    public WorkerEntity() {}

    public void setId(Long id) { Id = id; }

    public void setName(String name) { this.name = name; }

    public void setSurname(String surname) { this.surname = surname; }

    public void setWorkDays(int workDays) { this.workDays = workDays; }

    public void setStatus(String status) { this.status = status; }

    public void setLastVacationDate(Date lastVacationDate) { this.lastVacationDate = lastVacationDate; }

    public void setOnWork(Boolean isOnWork) { this.onWork = isOnWork; }

    public void setDepartmentId(int departmentId) { this.departmentId = departmentId; }

    public Long getId() { return Id; }

    public String getName() { return name; }

    public String getSurname() { return surname; }

    public int getWorkDays() { return workDays; }

    public String getStatus() { return status; }

    public Date getLastVacationDate() { return lastVacationDate; }

    public Boolean isOnWork() { return onWork; }

    public int getDepartmentId() { return departmentId; }
}
