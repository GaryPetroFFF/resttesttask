package db.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "departments")
public class DepartmentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "name")
    private String name;
    @OneToMany(mappedBy = "department_id")
    private Set<WorkerEntity> workerSet = new HashSet<>();

    public DepartmentEntity() {}

    public void setId(Long id) { this.id = id; }

    public void setName(String name) { this.name = name; }

    public void addWorker(WorkerEntity worker) { workerSet.add(worker); }

    public void deleteWorker(WorkerEntity worker) { workerSet.remove(worker); }

    public String getName() { return name; }

    public Long getId() { return id; }

    public Set<WorkerEntity> getWorkerSet() { return workerSet; }
}
