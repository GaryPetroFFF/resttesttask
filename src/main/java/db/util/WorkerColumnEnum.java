package db.util;

public enum WorkerColumnEnum {
    ID("id"),
    NAME("name"),
    SURNAME("surname"),
    WORK_DAYS("workDays"),
    STATUS("status"),
    LAST_VACATION_DATE("lastVacationDate"),
    IS_ON_WORK("onWork"),
    DEPARTMENT_ID("departmentId");

    private final String name;

    private WorkerColumnEnum(String name) { this.name = name; }

    public String getName() { return this.name; }
}
