package db.util;

public enum DepartmentColumnEnum {
    ID("id"),
    NAME("name");

    private final String name;

    DepartmentColumnEnum(String name) { this.name = name; }

    public String getName() { return name; }
}
