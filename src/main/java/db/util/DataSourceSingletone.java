package db.util;

import org.hibernate.cfg.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;


import javax.sql.DataSource;


public class DataSourceSingletone {
    private static DriverManagerDataSource dataSource;

    public static DataSource getDataSource() {
        if (dataSource == null) {
                Configuration configuration = new Configuration().configure();
                dataSource = new DriverManagerDataSource();
                dataSource.setDriverClassName(configuration.getProperty("hibernate.connection.driver_class"));
                dataSource.setUrl(configuration.getProperty("hibernate.connection.url"));
                dataSource.setUsername(configuration.getProperty("hibernate.connection.username"));
                dataSource.setPassword(configuration.getProperty("hibernate.connection.password"));
        }
        return dataSource;
    }
}

