package db.service;

import db.dao.WorkerDAO;
import db.dao.WorkerDAOImpl;
import db.model.WorkerEntity;

import java.util.List;

public class WorkerService {
    private WorkerDAO workerDAO = new WorkerDAOImpl();

    public WorkerService() {}

    public WorkerEntity findWorker(Long id) { return workerDAO.findById(id); }

    public void saveWorker(WorkerEntity worker) { workerDAO.save(worker); }

    public void updateWorker(WorkerEntity worker) { workerDAO.update(worker); }

    public void deleteWorker(WorkerEntity worker) { workerDAO.delete(worker); }

    public List<WorkerEntity> findAllWorkers() {
        return workerDAO.findAll();
    }
}
