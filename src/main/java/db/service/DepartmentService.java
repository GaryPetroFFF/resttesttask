package db.service;

import db.dao.DepartmentDAO;
import db.dao.DepartmentDAOImpl;
import db.model.DepartmentEntity;

import java.util.List;

public class DepartmentService {
    private DepartmentDAO mDepartmentDAO = new DepartmentDAOImpl();

    public DepartmentService() {}

    public DepartmentEntity findDepartment(Long id) { return mDepartmentDAO.findById(id); }

    public void saveDepartment(DepartmentEntity department) { mDepartmentDAO.save(department); }

    public void updateDepartment(DepartmentEntity department) { mDepartmentDAO.update(department); }

    public void deleteDepartment(DepartmentEntity department) { mDepartmentDAO.delete(department); }

    public List<DepartmentEntity> findAllDepartments() {
        return mDepartmentDAO.findAll();
    }
}
