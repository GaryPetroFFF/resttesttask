package controller;

import db.model.WorkerEntity;
import db.service.WorkerService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
public class WorkerController {

    @GetMapping(value = "/worker/{workerId}/work_days")
    public int getWorkerWorkDays(@PathVariable Long id) {
        WorkerEntity worker = (new WorkerService()).findWorker(id);
        return worker.getWorkDays();
    }

    @GetMapping(value = "/worker/{workerId}/last_vacation_date")
    public Date getWorkerLastVacationDate(@PathVariable Long id) {
        return (new WorkerService()).findWorker(id).getLastVacationDate();
    }

    @GetMapping(value = "/worker/{workerId}/{status}")
    public void setWorkerStatus(@PathVariable("workerId") Long id, @PathVariable("status") String status) {
        WorkerEntity worker = (new WorkerService()).findWorker(id);
        worker.setStatus(status);
        (new WorkerService()).saveWorker(worker);
    }

    @GetMapping(value = "/worker/{workerId}/{onWork}")
    public void setWorkerOnWork(@PathVariable("workerId") Long id, @PathVariable("onWork") boolean onWork) {
        WorkerEntity worker = (new WorkerService()).findWorker(id);
        worker.setOnWork(onWork);
        (new WorkerService()).saveWorker(worker);
    }
}
