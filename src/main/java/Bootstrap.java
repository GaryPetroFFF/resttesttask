import db.model.DepartmentEntity;
import db.model.WorkerEntity;
import db.service.DepartmentService;
import db.service.WorkerService;

import java.sql.SQLException;
import java.util.List;

public class Bootstrap {
    public static void main(String[] args) throws SQLException {
        WorkerService workerService = new WorkerService();
        DepartmentService departmentService = new DepartmentService();
        List<WorkerEntity> workerList = workerService.findAllWorkers();
        for (WorkerEntity worker: workerList) {
            if (worker.getName().equals("Test")) {
                workerService.deleteWorker(worker);
            }
        }
    }
}
