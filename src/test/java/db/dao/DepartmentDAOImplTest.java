package db.dao;

import db.model.DepartmentEntity;
import db.service.DepartmentService;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;

import static org.junit.Assert.*;

public class DepartmentDAOImplTest {
    private DepartmentService service = new DepartmentService();

    @Test
    public void save() {
        DepartmentEntity firstDepartment = new DepartmentEntity();
        firstDepartment.setName("First");

        service.saveDepartment(firstDepartment);
        DepartmentEntity secondDepartment = service.findDepartment(firstDepartment.getId());

        Assert.assertEquals(firstDepartment.getId(), secondDepartment.getId());
        Assert.assertEquals(firstDepartment.getName(), secondDepartment.getName());
        service.deleteDepartment(firstDepartment);
    }

    @Test
    public void update() {
        DepartmentEntity department = new DepartmentEntity();
        department.setName("Test");
        service.saveDepartment(department);

        department = service.findDepartment(department.getId());
        department.setName("Test2");
        service.saveDepartment(department);

        department = service.findDepartment(department.getId());
        Assert.assertEquals("Test2", department.getName());
        service.deleteDepartment(department);
    }

    @Test
    public void delete() {
        DepartmentEntity department = new DepartmentEntity();
        department.setName("Test");
        service.saveDepartment(department);
        final long id = department.getId();

        department = service.findDepartment(id);
        Assert.assertNotNull(department);

        service.deleteDepartment(department);

        Assert.assertThrows(EmptyResultDataAccessException.class, () -> service.findDepartment(id));
    }
}