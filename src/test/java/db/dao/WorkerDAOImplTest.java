package db.dao;

import db.model.WorkerEntity;
import db.service.WorkerService;
import org.junit.Assert;
import org.springframework.dao.EmptyResultDataAccessException;

import static org.junit.Assert.*;

public class WorkerDAOImplTest {
    private WorkerService service = new WorkerService();

    @org.junit.Test
    public void save() {
        WorkerEntity firstWorker = new WorkerEntity();
        firstWorker.setName("Test");
        firstWorker.setSurname("Test");
        firstWorker.setOnWork(true);
        firstWorker.setStatus("working");
        firstWorker.setWorkDays(25);

        service.saveWorker(firstWorker);

        WorkerEntity secondWorker = service.findWorker(firstWorker.getId());

        Assert.assertEquals(secondWorker.getId(), firstWorker.getId());
        Assert.assertEquals(secondWorker.getName(), firstWorker.getName());
        Assert.assertEquals(secondWorker.getSurname(), firstWorker.getSurname());
        Assert.assertEquals(secondWorker.isOnWork(), firstWorker.isOnWork());
        Assert.assertEquals(secondWorker.getStatus(), firstWorker.getStatus());
        Assert.assertEquals(secondWorker.getWorkDays(), firstWorker.getWorkDays());
        Assert.assertEquals(secondWorker.getLastVacationDate(), firstWorker.getLastVacationDate());
        Assert.assertEquals(secondWorker.getDepartmentId(), firstWorker.getDepartmentId());
        service.deleteWorker(firstWorker);
    }

    @org.junit.Test
    public void update() {
        WorkerEntity worker = new WorkerEntity();
        worker.setName("Test");
        worker.setSurname("Test");
        worker.setOnWork(true);
        worker.setStatus("working");
        worker.setWorkDays(25);

        service.saveWorker(worker);

        worker = service.findWorker(worker.getId());
        worker.setName("Test2");
        service.updateWorker(worker);
        worker = service.findWorker(worker.getId());

        Assert.assertEquals("Test2", worker.getName());
        service.deleteWorker(worker);
    }

    @org.junit.Test
    public void delete() {
        WorkerEntity worker = new WorkerEntity();
        worker.setName("Test");
        worker.setSurname("Test");
        worker.setOnWork(true);
        worker.setStatus("working");
        worker.setWorkDays(25);

        service.saveWorker(worker);
        final long id = worker.getId();

        worker = service.findWorker(id);
        Assert.assertNotNull(worker);

        service.deleteWorker(worker);

        Assert.assertThrows(EmptyResultDataAccessException.class, () -> service.findWorker(id));
    }
}